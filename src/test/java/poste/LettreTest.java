package poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LettreTest {

	private static float tolerancePrix = 0.001f;
	
	Lettre lettre1;
	Lettre lettre2;
	
	@BeforeEach
	void setUp() throws Exception {
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
	}

	//@Test
	//void testTarifRemboursement() {
	//	fail("Not yet implemented");
	//}

	@Test
	void testTarifAffranchissement() {
		assertEquals(1.0f, lettre1.tarifAffranchissement(), tolerancePrix);
		assertEquals(2.3f, lettre2.tarifAffranchissement(), tolerancePrix);
	}

	@Test
	void testToString() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire", lettre1.toString());
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence", lettre2.toString());
	}

}
