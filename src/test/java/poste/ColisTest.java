package poste;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;


class ColisTest {
	
	private static float tolerancePrix = 0.001f;
	Colis colis1;

	@BeforeEach
	void setUp() throws Exception {
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux,
				"train electrique", 200); 
	}

	//@Test
	//void testTarifRemboursement() {
	//	fail("Not yet implemented");
	//}

	@Test
	void testTarifAffranchissement() {
		assertEquals(3.5f, colis1.tarifAffranchissement(), tolerancePrix);
	}

	@Test
	void testToString() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0", colis1.toString());
	}

}
