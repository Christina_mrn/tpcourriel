package poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ColisExpressTest {

	private static float tolerancePrix = 0.001f;
	
	ColisExpress colisExpress1;
	
	@BeforeEach
	void setUp() throws ColisExpressInvalide {
		colisExpress1 = new ColisExpress("La mere Noel", "famille Artick, igloo 90, baie des vents",
				"7855", 20, 0.02f, Recommandation.deux, "train electrique", 2, true);
	}

	
	@Test
	void testCreationColisExpressInvalide() {
		assertThrows(ColisExpressInvalide.class, () -> {
			new ColisExpress("La mere Noel", "famille Artick, igloo 90, baie des vents",
				"7855", 4000, 0.02f, Recommandation.deux, "train electrique", 2, true);
		});
	}
	
	/*
	@Test(expected=ColisExpressInvalide.class)
	void testCreationColisExpressInvalide() throws ColisExpressInvalide{
			new ColisExpress("La mere Noel", "famille Artick, igloo 90, baie des vents",
				"7855", 400000, 0.02f, Recommandation.deux, "train electrique", 2, true);
	}*/
	
	@Test
	void testTarifAffranchissement() {
		assertEquals(33.0f, colisExpress1.tarifAffranchissement(), tolerancePrix);
	}

	@Test
	void testToString() {
		assertEquals("Colis express 7855/famille Artick, igloo 90, baie des vents/2/0.02/2.0/20.0/20160128", colisExpress1.toString());
	}

}
