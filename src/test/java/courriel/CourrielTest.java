package courriel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CourrielTest {
    
    Courriel courriel1;
    Courriel courriel2;
    Courriel courriel3;
    Courriel courriel4;

    @BeforeEach
    public void setUp() {
        courriel1 = new Courriel("christina@gmail.com","Test","Ceci est un message");
        courriel2 = new Courriel("julie_la_bg_du_34@icloud.com","Demande d'ami");
        courriel3 = new Courriel("M1k3F4n2M4ng4s@hotmail.fr","Documents � fournir","Voici la photocopie en PJ que vous m'aviez demand�.","photocopie.pdf");
        courriel4 = new Courriel("Juan..adresseemail@yahoo.fr","Ecoute cette musique !","Voici la musique en PJ");
    }
    
    @Test
    void testEstValideEmail() {
        assert(courriel1.estValideEmail());
        assert(courriel2.estValideEmail());
        assert(courriel3.estValideEmail());
        assertFalse(courriel4.estValideEmail());
    }

    @Test
    void testAvecPieceJointe() {
    	assertFalse(courriel1.avecPieceJointe());
    	assertFalse(courriel2.avecPieceJointe());
        assert(courriel3.avecPieceJointe());
        assertFalse(courriel4.avecPieceJointe());
    }

    @Test
    void testMotClePieceJointe() {
    	assertFalse(courriel1.motClePieceJointe(Courriel.motsCles));
    	assertFalse(courriel2.motClePieceJointe(Courriel.motsCles));
        assert(courriel3.motClePieceJointe(Courriel.motsCles));
        assert(courriel4.motClePieceJointe(Courriel.motsCles));
    }

    @Test
    void testConformePieceJointe() {
    	assertFalse(courriel1.conformePieceJointe());
    	assertFalse(courriel2.conformePieceJointe());
        assert(courriel3.conformePieceJointe());
        assertFalse(courriel4.conformePieceJointe());
    }

    @Test
    void testEnvoyer() {
        assert(courriel1.envoyer());
        assert(courriel2.envoyer());
        assert(courriel3.envoyer());
        assertFalse(courriel4.envoyer());
    }

}