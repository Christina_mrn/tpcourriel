package courriel;

import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Courriel {

	private String email;
	private String titre;
	private String message;
	private String pieceJointe;
	
	public static List<String> motsCles = new ArrayList<>(List.of("PJ", "joint", "jointe"));
	private String regex = "^[a-zA-Z]\\w*@[a-zA-Z]+\\.[a-zA-Z]+$";
	
	public Courriel() {
		this.email = new String();
		this.titre = new String();
		this.message = new String();
		this.pieceJointe = new String();
	}
	
	//Un email peut ne pas avoir de message, ce n'est pas precise dans la consigne
	public Courriel(String email, String titre) {
		this();
		this.email = email;
		this.titre = titre;
	}
	
	//De meme, pas oblige de mettre la pieceJointe
	public Courriel(String email, String titre, String message) {
		this(email, titre);
		this.message = message;
	}
	
	//Un complet
	public Courriel(String email, String titre, String message, String pieceJointe) {
		this(email, titre, message);
		this.pieceJointe = pieceJointe;
	}
	
	
	/** GET and SET **/
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getPieceJointe() {
		return pieceJointe;
	}

	public void setPieceJointe(String pieceJointe) {
		this.pieceJointe = pieceJointe;
	}
	
	
	/** Methodes **/
	
	
	public boolean estValideEmail(){
		Pattern pattern = Pattern.compile(regex/*, Pattern.CASE_INSENSITIVE*/);
		Matcher matcher = pattern.matcher(email);
		//return matcher.find();
		return matcher.matches();
	}
	
	public boolean avecPieceJointe() {
		return !pieceJointe.isBlank();
	}
	
	public boolean motClePieceJointe(List<String> listeMotCles) {
		for (String str : listeMotCles) {
			if (message.contains(str)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean conformePieceJointe() {
		return avecPieceJointe() && motClePieceJointe(motsCles);
	}

	public boolean envoyer() {
		
		if (!estValideEmail()) {
			return false;
		}
		
		if (this.titre.isBlank()) {
			return false;
		}
		
		if (avecPieceJointe() && !conformePieceJointe()) {
			return false;
		}

		return true;
		
	}
	
	public String toString() {
		return this.email + "/"
				+ this.titre + "/"
				+ this.message + "/"
				+ this.pieceJointe + "/";
		//return "------------\nEmail : "+getEmail()+" (email valide ? "+estValideEmail()+")\nObjet : "+getTitre()+" (Pièce Jointe ? "+avecPieceJointe()+" "+getPieceJointe()+") \n\nMessage : "+getMessage()+"\n\n[Mot Clé ? "+motClePieceJointe(motsCles)+"]\nPièce jointe conforme ? "+conformePieceJointe()+"\nConforme envoi ? "+envoyer()+"\n\n";
	}
	
	public void affiche() {
		System.out.println(toString());
	}


}
